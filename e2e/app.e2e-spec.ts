import { ArpRallyPage } from './app.po';

describe('arp-rally App', () => {
  let page: ArpRallyPage;

  beforeEach(() => {
    page = new ArpRallyPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
