import {Routes, RouterModule} from '@angular/router';


// Components
import {DashboardComponent} from './components/admin/dashboard/dashboard.component';
import {LandingComponent} from './components/landing/landing.component';
import {ParticipantsComponent} from './components/participants/participants.component';
import {ScoringComponent} from './components/scoring/scoring.component';
import {ParticipantComponent} from './components/admin/participant/participant.component';
import {RallyComponent} from './components/admin/rally/rally.component';
import {RallyTimerComponent} from './components/rally-timer/rally-timer.component';


const appRoutes: Routes = [
    {path: '', component: LandingComponent},
    {path: 'landing', redirectTo: ''},
    {path: 'participants', component: ParticipantsComponent},
    {path: 'scoring', component: ScoringComponent},
    {path: 'admin/dashboard', component: DashboardComponent},
    {path: 'admin/participant', component: ParticipantComponent},
    {path: 'admin/rally', component: RallyComponent},
    {path: 'rallytimer', component: RallyTimerComponent},
    // otherwise redirect to home
    {path: '**', redirectTo: ''}
];


export const routing = RouterModule.forRoot(appRoutes);