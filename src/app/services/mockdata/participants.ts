import {Participant} from '../../models/participant'

export const PARTICIPANTS: Participant[] = [
    {
        createdAt: '',
        createdBy: '',
        startTime: 0,
        endTime: 0,
        bonusPoints: 0,
        car: {
            make: 'Alfa Romeo',
            model: 'Giulietta',
            engine: '1.8 TBI 270PS',
            licensePlatePart1: 'B',
            licensePlatePart2: 'QV',
            licensePlatePart3: '400',
            startingNumber: 1
        },
        driver: {
            firstName: 'Andre',
            lastName: 'Schubert',
            age: 38,
            email: 'andre@alfa-romeo-portal.de'
        },
        codriver: {
            firstName: 'Andre',
            lastName: 'Schubert',
            age: 38,
            email: 'andre@alfa-romeo-portal.de'
        }
    },
    {
        createdAt: '',
        createdBy: '',
        startTime: 0,
        endTime: 0,
        bonusPoints: 0,
        car: {
            make: 'Alfa Romeo',
            model: 'GTV',
            engine: '3.2 V6 220PS',
            licensePlatePart1: 'B',
            licensePlatePart2: 'V',
            licensePlatePart3: '6916',
            startingNumber: 1
        },
        driver: {
            firstName: 'Andre',
            lastName: 'Schubert',
            age: 38,
            email: 'andre@alfa-romeo-portal.de'
        },
        codriver: {
            firstName: 'Andre',
            lastName: 'Schubert',
            age: 38,
            email: 'andre@alfa-romeo-portal.de'
        }
    },
];