import {Injectable} from '@angular/core';
import {Http, Response, Headers, RequestOptions} from "@angular/http";

import {Participant} from '../models/participant';
import {PARTICIPANTS} from './mockdata/participants';

import {Observable} from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class ParticipantsService {

    private headers = new Headers({'Content-Type': 'application/json'});
    private participantsUrl = 'https://ta2v60nlf7.execute-api.eu-central-1.amazonaws.com/development/participants';
    private participantUrl = 'https://ta2v60nlf7.execute-api.eu-central-1.amazonaws.com/development/participant';


    constructor(private http: Http) {
    }


    getParticipants(): Observable<Participant[]> {
        return this.http.get(this.participantsUrl)
            .map((res: Response) => res.json() as Participant[])
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));

    }


    addParticipant(model:Participant): Observable<Participant> {
        return this.http.post(this.participantUrl, model)
            .map((res: Response) => res.json() as Participant)
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));

    }


    updateParticipant(model:Participant): Observable<Participant> {
        return this.http.put(this.participantUrl, model)
            .map((res: Response) => res.json() as Participant)
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));

    }


    deleteParticipant(docKey:string): Observable<Participant> {
        return this.http.delete(this.participantUrl + '/CAR_2017_' + docKey)
            .map((res: Response) => res.json() as Participant)
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));

    }
}