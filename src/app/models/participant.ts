import { Car } from './car';
import { Driver } from './driver';

export class Participant {
    createdAt: string;
    createdBy: string;

    startTime?: number;
    endTime?: number;

    bonusPoints?: number;

    driver: Driver;
    codriver?: Driver;

    car: Car;
}
