export class Driver {
    firstName: string;
    lastName?: string;
    age?: number;
    email?: string;
}