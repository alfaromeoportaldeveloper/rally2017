export class Car {
    make?: string;
    model?: string;
    engine?: string;
    licensePlatePart1: string;
    licensePlatePart2: string;
    licensePlatePart3: string;
    startingNumber: number;
}
