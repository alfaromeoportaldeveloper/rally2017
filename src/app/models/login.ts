export class Login {
    email: string;
    password: string;
    authenticated?: boolean
}