import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {ParticipantsService} from '../../../services/participants.service';
import {Participant} from "../../../models/participant";

@Component({
    selector: 'app-rally',
    templateUrl: './rally.component.html',
    styleUrls: ['./rally.component.css'],
    providers: [ParticipantsService]
})
export class RallyComponent implements OnInit {

    constructor(private router: Router, private participantsService: ParticipantsService) {
    }

    participants: Participant[];
    showLoading: boolean = false;

    ngOnInit() {
        if (localStorage.getItem('authenticated')) {
            console.log('granted access to admin dashboard...');

            this.getParticipants();

        } else {
            console.log('access denied for admin dashboard!');
            this.router.navigate(['landing']);
        }
    }


    getParticipants(): void {

        console.log('try to get participants...');
        this.showLoading = true;

        this.participantsService.getParticipants()
            .subscribe(
                participants => {

                    console.log('SUCCESS: got participants from service...');

                    this.participants = participants;
                    this.showLoading = false;

                },
                err => {
                    // Log errors if any
                    console.log('ERROR: ', err);
                    this.showLoading = false;
                }
            );
    }

    startRally(participant: Participant): void {
        participant.startTime = Date.now();
        this.showLoading = true;

        this.participantsService.updateParticipant(participant)
            .subscribe(
                participant => {
                    console.log('SUCCESS:updated new participant, set startTime ...');
                    this.showLoading = false;
                },
                err => {
                    // Log errors if any
                    console.log('ERROR: ', err);
                    this.showLoading = false;
                }
            );
    }

    stopRally(participant: Participant): void {
        participant.endTime = Date.now();
        this.showLoading = true;

        this.participantsService.updateParticipant(participant)
            .subscribe(
                participant => {
                    console.log('SUCCESS:updated new participant, set stopTime ...');
                    this.showLoading = false;
                },
                err => {
                    // Log errors if any
                    console.log('ERROR: ', err);
                    this.showLoading = false;
                }
            );
    }

    resetTimer(participant: Participant): void {

        this.showLoading = true;

        participant.startTime = null;
        participant.endTime = null;
        participant.bonusPoints = 0;

        this.participantsService.updateParticipant(participant)
            .subscribe(
                participant => {
                    console.log('SUCCESS:updated new participant, deleted startTime and endTime ...');
                    this.showLoading = false;

                    // this.getParticipants();
                },
                err => {
                    // Log errors if any
                    console.log('ERROR: ', err);
                    this.showLoading = false;
                }
            );
    }

    updateBonuspoints(participant: Participant): void {
        this.showLoading = true;

        if (participant.bonusPoints > 0) {
            this.participantsService.updateParticipant(participant)
                .subscribe(
                    participant => {
                        console.log('SUCCESS:updated participant, updated bonuspoints ...');
                        this.showLoading = false;

                        // this.getParticipants();
                    },
                    err => {
                        // Log errors if any
                        console.log('ERROR: ', err);
                        this.showLoading = false;
                    }
                );
        } else {
            this.showLoading = false;
        }
    };

}
