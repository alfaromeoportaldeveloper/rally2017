import {Component, OnInit} from '@angular/core';
import {ParticipantsService} from '../../../services/participants.service'
import {Router}   from '@angular/router';
import {Participant} from "../../../models/participant";
import {Car} from '../../../models/car';
import {Driver} from '../../../models/driver';

@Component({
    selector: 'app-participant',
    templateUrl: './participant.component.html',
    styleUrls: ['./participant.component.css'],
    providers: [ParticipantsService]
})
export class ParticipantComponent implements OnInit {

    constructor(private participantsService: ParticipantsService, private router: Router) {
    }

    showSuccess: boolean = false;
    showError: boolean = false;
    showLoading:boolean = false;

    participant: Participant;

    createParticipant(form) {

        if(form && form.form._status === 'INVALID'){
            this.showError = true;
            return;
        }


        console.log('called createParticipant()....', this.participant);

        this.showLoading = true;

        if (this.participant.car &&
            this.participant.car.licensePlatePart1 &&
            this.participant.car.licensePlatePart2 &&
            this.participant.car.licensePlatePart3) {

            this.participantsService.addParticipant(this.participant)
                .subscribe(
                    participant => {
                        console.log('SUCCESS:saved new participant...');
                        this.initParticipantModel();
                        this.showSuccess = true;
                        this.showLoading = false;
                    },
                    err => {
                        // Log errors if any
                        console.log('ERROR: ', err);
                        this.showError = true;
                        this.showLoading = false;
                    }
                );
        } else {
            this.showError = true;
        }


    }

    initParticipantModel() {
        this.participant = new Participant();
        this.participant.createdAt = new Date().toString();
        this.participant.createdBy = 'ARP-Admin';
        this.participant.car = new Car();
        this.participant.driver = new Driver();
        this.participant.codriver = new Driver();
    }


    resetSuccessNotice(){
        this.showSuccess = false;
    }

    resetErrorNotice(){
        this.showError = false;
    }


    ngOnInit() {
        if (localStorage.getItem('authenticated')) {
            console.log('granted access to admin dashboard...');

            this.initParticipantModel();

        } else {
            console.log('access denied for admin dashboard!');
            this.router.navigate(['landing']);
        }
    }

}
