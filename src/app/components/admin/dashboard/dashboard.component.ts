import { Component, OnInit } from '@angular/core';
import { Router }   from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
      if(localStorage.getItem('authenticated')){
          console.log('granted access to admin dashboard...');
      } else {
          console.log('access denied for admin dashboard!');
          this.router.navigate(['landing']);
      }
  }

}
