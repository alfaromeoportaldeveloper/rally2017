import {Component, OnInit, Input, OnChanges, SimpleChanges, OnDestroy} from '@angular/core';
import {Subscription} from "rxjs";
import {TimerObservable} from "rxjs/observable/TimerObservable";

@Component({
    selector: 'app-rally-timer',
    templateUrl: './rally-timer.component.html',
    styleUrls: ['./rally-timer.component.css']
})
export class RallyTimerComponent implements OnInit, OnChanges, OnDestroy {


    constructor() {
    }

    @Input()
    startTime: number = 0;

    @Input()
    stopTime: number = 0;

    counterTime: number = 0;
    private subscription: Subscription;

    ngOnInit() {

        let d = new Date();
        d.setHours(0,0,0,0);

        this.counterTime = d.getTime();

    }

    ngOnChanges(changes: SimpleChanges): void {
        console.log('sth changed in timer component...', changes);

        // start counter
        if(changes.startTime && !changes.startTime.firstChange){
            this.startTime = changes.startTime.currentValue;

            let timer = TimerObservable.create(0, 1000);

            this.subscription = timer.subscribe(t => {
                this.counterTime = this.counterTime + 1000;
            });
        }

        // stop counter
        if(changes.stopTime && !changes.stopTime.firstChange && changes.stopTime.currentValue > this.startTime){
            this.subscription.unsubscribe();
        }
    }


    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }

}
