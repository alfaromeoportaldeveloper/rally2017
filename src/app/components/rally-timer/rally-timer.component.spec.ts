import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RallyTimerComponent } from './rally-timer.component';

describe('RallyTimerComponent', () => {
  let component: RallyTimerComponent;
  let fixture: ComponentFixture<RallyTimerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RallyTimerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RallyTimerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
