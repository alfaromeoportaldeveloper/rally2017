import {Component, OnInit} from '@angular/core';
import {Login} from '../../models/login'
import {Router}   from '@angular/router';


@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

    login: Login = new Login();

    constructor(private router: Router) {
    }

    public isCollapsed:boolean = true;

    public collapsed(event:any):void {
        console.log(event);
    }

    public expanded(event:any):void {
        console.log(event);
    }


    loginAdmin(): void {
        console.log('got a login request...', this.login);

        if (this.login.email === 'admin@alfa-romeo-portal.de' && this.login.password === '!harz2017rally#') {
            console.log('login successfull....');

            this.login.authenticated = true;

            localStorage.setItem('authenticated', JSON.stringify(this.login));
            this.router.navigate(['admin/dashboard']);
        } else {
            console.error('login was not successfull!');
        }
    }

    logout(): void {
        console.log('try to logout user...', this.login);
        this.login = new Login();
        localStorage.removeItem('authenticated');
        this.router.navigate(['landing']);
    }

    ngOnInit() {
        if(localStorage.getItem('authenticated')){
            this.login = <Login>JSON.parse(localStorage.getItem('authenticated'));
        }
    }

}
