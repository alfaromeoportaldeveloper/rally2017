import {Component, OnInit} from '@angular/core';
import {Participant} from "../../models/participant";
import {ParticipantsService} from '../../services/participants.service';
import {SlimLoadingBarService} from 'ng2-slim-loading-bar';
import {Login} from "../../models/login";

@Component({
    selector: 'app-participants',
    templateUrl: './participants.component.html',
    styleUrls: ['./participants.component.css'],
    providers: [ParticipantsService]
})
export class ParticipantsComponent implements OnInit {

    constructor(private participantsService: ParticipantsService, private slimLoadingBarService: SlimLoadingBarService) {
    }

    participants: Participant[];

    showSuccess: boolean = false;
    showError: boolean = false;
    showLoading:boolean = false;

    login: Login;

    ngOnInit(): void {
        this.getParticipants();
    }

    getParticipants(): void {

        this.showLoading = true;

        console.log('try to get participants...');

        this.participantsService.getParticipants()
            .subscribe(
                participants => {
                    console.log('SUCCESS: got participants from service...');
                    this.participants = participants;
                    // this.showSuccess = true;
                    this.showLoading = false;
                },
                err => {
                    // Log errors if any
                    console.log('ERROR: ', err);
                    this.showError = true;
                    this.showLoading = false;
                }
            );

    }


    deleteParticipant(participant:Participant): void {
        console.log('try to delete participants...');

        this.showLoading = true;

        let docKey:string = participant.car.licensePlatePart1 + '_' + participant.car.licensePlatePart2 + '_' + participant.car.licensePlatePart3;

        this.participantsService.deleteParticipant(docKey)
            .subscribe(
                participant => {
                    console.log('SUCCESS: deleted participant in database...');
                    this.showSuccess = true;

                    this.getParticipants();
                    this.showLoading = false;
                },
                err => {
                    // Log errors if any
                    console.log('ERROR: ', err);
                    this.showError = true;
                    this.showLoading = false;
                }
            );
    }


    isAdmin(): boolean {
        this.login = JSON.parse(localStorage.getItem('authenticated'));
        if(this.login){
            return this.login.authenticated;
        }else{
            return false;
        }
    }


    resetSuccessNotice(){
        this.showSuccess = false;
    }

    resetErrorNotice(){
        this.showError = false;
    }

}
