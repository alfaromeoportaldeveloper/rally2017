import {Component, OnInit} from '@angular/core';
import {ParticipantsService} from '../../services/participants.service';
import {Participant} from "../../models/participant";

@Component({
    selector: 'app-scoring',
    templateUrl: './scoring.component.html',
    styleUrls: ['./scoring.component.css'],
    providers: [ParticipantsService]
})
export class ScoringComponent implements OnInit {

    constructor(private participantsService: ParticipantsService) {
    }

    participants: Participant[];

    showLoading:boolean = false;

    ngOnInit() {
        this.getParticipants();
    }


    getParticipants(): void {
        console.log('try to get participants...');

        this.showLoading = true;

        this.participantsService.getParticipants()
            .subscribe(
                participants => {
                    console.log('SUCCESS: got participants from service...');
                    this.participants = participants;
                    this.showLoading = false;
                },
                err => {
                    // Log errors if any
                    console.log('ERROR: ', err);
                    this.showLoading = false;
                }
            );
    }

}
