import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'orderByTimePipe'
})
export class OrderByTimePipe implements PipeTransform{

    transform(array: Array<string>, args: string): Array<string> {

        if(!array || array === undefined || array.length === 0) return null;

        array.sort((a: any, b: any) => {

            if(!a.startTime || !b.startTime){
                return 1;
            }

            if (a.endTime - a.startTime < b.endTime - b.startTime) {
                return -1;
            } else if (a.endTime - a.startTime > b.endTime - b.startTime) {
                return 1;
            } else {
                return 0;
            }
        });

        return array;
    }

}