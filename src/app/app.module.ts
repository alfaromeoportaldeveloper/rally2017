import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {RouterModule} from '@angular/router';
import {CollapseModule} from 'ngx-bootstrap/collapse';
import {AlertModule} from 'ngx-bootstrap/alert';
import {SlimLoadingBarModule} from 'ng2-slim-loading-bar';
import {routing} from './app.routing';

import {AppComponent} from './app.component';
import {HeaderComponent} from './components/header/header.component';
import {FooterComponent} from './components/footer/footer.component';
import {ScrollToModule} from 'ng2-scroll-to';
import {DashboardComponent} from './components/admin/dashboard/dashboard.component';
import {LandingComponent} from './components/landing/landing.component';
import {ParticipantsComponent} from './components/participants/participants.component';
import {ScoringComponent} from './components/scoring/scoring.component';
import {ParticipantComponent} from './components/admin/participant/participant.component';
import {RallyComponent} from './components/admin/rally/rally.component'
import {RallyTimerComponent} from './components/rally-timer/rally-timer.component';
import {OrderByPipe} from "./pipes/orderby.pipe";
import {OrderByTimePipe} from "./pipes/orderbytime.pipe";
import {HashLocationStrategy, LocationStrategy} from '@angular/common';


@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        FooterComponent,
        DashboardComponent,
        LandingComponent,
        ParticipantsComponent,
        ScoringComponent,
        ParticipantComponent,
        RallyComponent,
        OrderByPipe,
        OrderByTimePipe,
        RallyTimerComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        routing,
        ScrollToModule.forRoot(),
        CollapseModule.forRoot(),
        AlertModule.forRoot(),
        SlimLoadingBarModule.forRoot()
    ],
    providers: [{provide: LocationStrategy, useClass: HashLocationStrategy}],
    bootstrap: [AppComponent]
})
export class AppModule {
}
